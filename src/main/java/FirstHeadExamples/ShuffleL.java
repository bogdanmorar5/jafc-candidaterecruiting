package FirstHeadExamples;

public class ShuffleL {
    public static void main(String[] args) {
        int number = 3;
        while (number > 0) {
            if (number > 2) {
                System.out.print("a");
            }
            number = number - 1;
            System.out.print("-");
            if(number == 2) {

                System.out.print("b c");
            }
            if(number == 1) {
                System.out.print("d");
                number = number - 1;
            }
        }
    }
}
