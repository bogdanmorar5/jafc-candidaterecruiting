package FirstHeadExamples;

public class PhareOMatic {
    public static void main(String[] args) {
        String [] firstString = {"win-win", "smart", "30,000 foot"};
        String [] secondString = {"focused", "brand", "sticky"};
        String [] thirdString = {"vision", "dispute", "process"};

        int firstStringLength = firstString.length;
        int secondStringLength = secondString.length;
        int thirdStringLength = thirdString.length;

        int generateRandomOne = (int) (Math.random() * firstStringLength);
        int generateRandomTwo = (int) (Math.random() * secondStringLength);
        int generateRandomTree = (int) (Math.random() * thirdStringLength);

        String phase = firstString[generateRandomOne] + " " + secondString[generateRandomTwo] + " " +
                thirdString[generateRandomTree];

        System.out.println("What we need is a: " + phase);
    }
}
