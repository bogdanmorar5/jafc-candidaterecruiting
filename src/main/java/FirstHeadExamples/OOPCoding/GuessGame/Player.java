package FirstHeadExamples.OOPCoding.GuessGame;

public class Player {
    int number = 0;     //valoarea de la care pleaca guessingul

    // se creaza o metoda pentru a genera numerele random
    public void guess() {
        number = (int) (Math.random() * 10);
        System.out.println("I'm guessing " + number); // totul e abstract, nu trebuie specificat care player ghiceste
        // si ce ghiceste, GENERALIZAM TOTUL
    }
}
