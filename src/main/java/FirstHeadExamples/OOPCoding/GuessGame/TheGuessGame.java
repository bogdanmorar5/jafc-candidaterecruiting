package FirstHeadExamples.OOPCoding.GuessGame;

public class TheGuessGame {

    Player p1;  //S-a creat clasa pentru player
    Player p2;
    Player p3;

    //S-a instantat variabila player
    public void startGame() {
        p1 = new Player();
        p2 = new Player();
        p3 = new Player();

        //Am declarat valorile pentru cei trei jucatori
        int guessP1 = 0;
        int guessP2 = 0;
        int guessP3 = 0;

        //Am declarat 3 variabile care sa tina true sau false bazat pe raspunsul playerilor.
        boolean p1IsRight = false;
        boolean p2IsRight = false;
        boolean p3IsRight = false;

        int targetNumber = (int) (Math.random() * 10);    // un target number that players have to guess
        System.out.println("I'm think of a number between 0 and 9 ...");
        //Math.random() trebuie sa fie neaparat in paranteza altfel o sa-l rotunjeasca catre 0 intotdeauna

        while(true) {
            System.out.println("Number to guess is: " + targetNumber);

            p1.guess();
            p2.guess();
            p3.guess();

            guessP1 = p1.number;
            System.out.println("Player one guessed " + guessP1);

            guessP2 = p2.number;
            System.out.println("Player two guessed " + guessP2);

            guessP3 = p3.number;
            System.out.println("Player three guessed " + guessP3);

            if(guessP1 == targetNumber) {
                p1IsRight = true;
            }
            if(guessP2 == targetNumber) {
                p2IsRight = true;
            }
            if (guessP3 == targetNumber) {
                p3IsRight = true;
            }

            if(p1IsRight || p2IsRight || p3IsRight) {
                System.out.println("The winner is: ");
                System.out.println("Player one got it right ?" + p1IsRight);
                System.out.println("Player two got it right ?" + p2IsRight);
                System.out.println("Player three got it right ?" + p3IsRight);
                System.out.println("Game is over");
                break;
            } else {
                System.out.println("Players will have to try again. Better luck next time!");
            }

        }

    }
}
