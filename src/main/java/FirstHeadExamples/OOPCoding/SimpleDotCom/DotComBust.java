package FirstHeadExamples.OOPCoding.SimpleDotCom;

import java.util.ArrayList;

public class DotComBust {

    private GameHelper helper = new GameHelper();
    private ArrayList<DotCom> dotComList = new ArrayList<DotCom>();
    private int numberOfGuesses = 0;


    public void setUpGame() {
        DotCom one = new DotCom();
        one.setName("Pets.com");
        DotCom two = new DotCom();
        two.setName("eToys.com");
        DotCom three = new DotCom();
        three.setName("Go2.com");

        dotComList.add(one);
        dotComList.add(two);
        dotComList.add(three);

        System.out.println("Your goal is to sink three dot coms.");
        System.out.println("Pets.com, eToys.com, Go2.com.");
        System.out.println("Try to sink them all in the fewest number of guesses!");

        for (DotCom dotComToSet : dotComList) {
            ArrayList<String> newLocation = helper.placeDotCom(3);
            dotComToSet.setLocationCells(newLocation);
        }
    }

    public void playGame() {

        while (!dotComList.isEmpty()) {
            String userGuess = helper.getUserInput("Enter a guess:");
            checkUserGuess(userGuess);
        }
        finishGame();
    }

    public void checkUserGuess(String userGuess) {

        numberOfGuesses++;
        String result = "miss";

        for(DotCom dotComeToTest : dotComList) {
            result = dotComeToTest.checkYourself(userGuess);
            if(result.equals("hit")) {
                break;
            }
            if(result.equals("kill")) {
                dotComList.remove(dotComeToTest);
                break;
            }
        }
        System.out.println(result);
    }

    public void finishGame() {
        System.out.println("All DotComs are dead! Your stock is now worthless");
        if(numberOfGuesses <= 18) {
            System.out.println("It took only " + numberOfGuesses + " guesses.");
            System.out.println("You got out before your options sank");
        } else {
            System.out.println("Took you lonk enough " + numberOfGuesses + "guesses.");
            System.out.println("Fish are dancing with your options");
        }
    }
}