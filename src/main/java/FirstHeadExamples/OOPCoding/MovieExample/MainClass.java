package FirstHeadExamples.OOPCoding.MovieExample;

public class MainClass {
    public static void main(String[] args) {
        Movie movieOne = new Movie();

        movieOne.genre = "Tragic";
        movieOne.name = "Gone with the stock";
        movieOne.rating = 8;

        Movie movieTwo = new Movie();
        movieTwo.genre = "Comedy";
        movieTwo.name = "Lost in the Cubicle Space";
        movieTwo.rating = 5;

        movieOne.playIt();
        movieTwo.playIt();


    }
}
