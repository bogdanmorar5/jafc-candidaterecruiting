package FirstHeadExamples.OOPCoding.PoolPuzzle.PuzzlePAG44;

public class EchoTestDrive {
    public static void main(String[] args) {

        int number = 0;

        Echo e1 = new Echo();
        Echo e2 = new Echo();

        while (number < 4) {
            e1.hello();
            e1.count = e1.count + 1;

            if(number == 3) {
                e2.count = e2.count + 1;
            }
            if(number > 0) {
                e2.count = e2.count + e1.count;
            }
            number++;
        }

        System.out.println(e2.count);

    }
}
