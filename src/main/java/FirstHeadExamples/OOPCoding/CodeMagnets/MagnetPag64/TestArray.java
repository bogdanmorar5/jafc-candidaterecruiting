package FirstHeadExamples.OOPCoding.CodeMagnets.MagnetPag64;

import java.sql.SQLOutput;
import java.util.Arrays;

public class TestArray {
    public static void main(String[] args) {

        int [] index = new int[4];

        index[0] = 1;
        index[1] = 3;
        index[2] = 0;
        index[3] = 2;

        String [] islands = new String[4];

        islands[0] = "Bermuda";
        islands[1] = "Fiji";
        islands[2] = "Azores";
        islands[3] = "Cozumel";

        int y = 0;
        while(y < index.length && y < islands.length) {

            int ref;
            ref = index[y];
            System.out.print("island = " );
            System.out.println(islands[ref]);
            y++;

        }
    }
}
