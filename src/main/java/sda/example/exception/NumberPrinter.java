package sda.example.exception;

public interface NumberPrinter {
    public void printNumber(String number);
}
