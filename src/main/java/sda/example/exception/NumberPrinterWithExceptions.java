package sda.example.exception;

public class NumberPrinterWithExceptions implements NumberPrinter {

    @Override
    public void printNumber(String number) {

        String number1 = "1234";
        int number1Int = Integer.parseInt(number);

        boolean numberFound = false;

        int numberInt = 0;
        try {
            numberInt = getInt(number);
            System.out.println("int: " + numberInt);
            numberFound = true;
        } catch (NotANumberException e) {
        }

        if (!numberFound) {
            try {
                double numberDouble = Double.parseDouble(number);
                System.out.println("double: " + numberDouble);
            } catch (NumberFormatException ex2) {
                System.out.println("Please enter a numeric value!");
            }
        }
    }

    private int getInt(String number) throws NotANumberException {
        int result = 0;
        try {
            result = Integer.parseInt(number);
        } catch (NumberFormatException ex) {
            throw new NotANumberException("Numbarul trimis nu este un int!");
        }
        return result;
    }

}
