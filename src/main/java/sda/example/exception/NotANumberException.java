package sda.example.exception;

public class NotANumberException extends Exception {

    public NotANumberException(String message) {
        super(message);
    }

    public NotANumberException(String message, Throwable cause) {
        super(message, cause);
    }

}
