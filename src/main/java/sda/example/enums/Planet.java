package sda.example.enums;

import java.util.NoSuchElementException;

public class Planet {

    public static Planet PLUTO = new Planet("PLUTO", "It's bigger!", 101);
    public static Planet EARTH = new Planet("EARTH", "It's the same!", 104);

    private final String name;

    private final String marime;
    private final int distance;

    private Planet(String name, String marime, int distance) {
        this.name = name;
        this.marime = marime;
        this.distance = distance;
    }

    public static Planet valueOf(String planetName) {
        if (planetName.equalsIgnoreCase(PLUTO.name)) {
            return PLUTO;
        } else if (planetName.equalsIgnoreCase(EARTH.name)) {
            return EARTH;
        } else {
            throw new NoSuchElementException("Nu exista planeta ceruta !!!");
        }
    }

    public static Planet[] values() {
        return new Planet[]{PLUTO, EARTH};
    }

    public String name() {
        return name;
    }

}
