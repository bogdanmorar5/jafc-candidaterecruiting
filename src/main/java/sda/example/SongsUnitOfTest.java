package sda.example;

import java.util.Random;

public class SongsUnitOfTest {

    public static void main(String[] args) {
        Song[] songs = new Song[2];
        songs[0] = new LaLaSong(Gama.DO_MAJOR);
        songs[1] = new TriluRiluSong(Gama.RE_MINOR);
//        songs[0] = new Song();
//        songs[0] = new AnySong();
//        songs[0] = new Trompet();

        Instrument[] allInstruments = new Instrument[10];
        allInstruments[0] = new Piano();
        allInstruments[1] = new Trompet();
        allInstruments[2] = new Piano();
        allInstruments[3] = new Trompet();
        allInstruments[4] = new Trompet();
        allInstruments[5] = new Trompet();
        allInstruments[6] = new Piano();
        allInstruments[7] = new Trompet();
        allInstruments[8] = new Piano();
        allInstruments[9] = new Trompet();

        Random random = new Random();

        for (Song song: songs) {
            System.out.println("Name: " + song.getName());
            System.out.println("NameLenght: " + song.getNameLenght());
            System.out.println("Length: " + song.getLenght());

            int instrumentIndex = random.nextInt(allInstruments.length);
            Instrument instrument = allInstruments[instrumentIndex];

//            boolean isPiano = random.nextBoolean();
//            if (isPiano) {
//                instrument = allInstruments[0];
//            } else {
//                instrument = allInstruments[1];
//            }
            song.play(instrument);

            System.out.println("-------------------------------------");
        }
    }

}
