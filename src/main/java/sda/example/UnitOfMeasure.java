package sda.example;

public enum UnitOfMeasure {
    MM(1, "millimeters"),
    CM(10, "centimeters");

    final private int factor;
    final private String longName;

    private UnitOfMeasure (int factor, String longName) {
        this.factor = factor;
        this.longName = longName;
    }

    public int getDimensionInNewUM(UnitOfMeasure newUM, int dimension) {
        return dimension * (this.factor / newUM.factor);
    }

    @Override
    public String toString() {
        return longName;
    }

}
