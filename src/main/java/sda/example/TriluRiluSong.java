package sda.example;

public class TriluRiluSong extends AnySong {

    private static Note[] notes = new Note[]{Note.MI, Note.FA, Note.SOL, Note.MI, Note.RE, Note.SOL, Note.MI, Note.LA, Note.FA, Note.RE};

    private Gama gama;

    TriluRiluSong(Gama gama) {
        super(TriluRiluSong.class.getSimpleName(), notes.length);
        this.gama = gama;
    }

    @Override
    public Gama getGama() {
        return gama;
    }
    @Override
    public Note[] getNotes() {
        return notes;
    }

}
