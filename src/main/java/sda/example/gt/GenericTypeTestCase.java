package sda.example.gt;

public class GenericTypeTestCase {
    public static void main(String[] args) {
        Person p1 = new Person("Popescu Vasile", 20, 182);
        Person p2 = new Person("Pop Marin", 35, 178);

        if (p1.compareTo(p2) > 0) {
            System.out.println(p1.getName() + " este mai inalt decat " + p2.getName());
        } else if (p1.compareTo(p2) < 0) {
            System.out.println(p1.getName() + " este mai scund decat " + p2.getName());
        } else {
            System.out.println(p1.getName() + " este la fel de inalt ca si " + p2.getName());
        }
    }
}
