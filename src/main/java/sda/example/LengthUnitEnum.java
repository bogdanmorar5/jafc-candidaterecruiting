package sda.example;

public class LengthUnitEnum {
    public static void main(String[] args) {

       LengthUnit unitOf = LengthUnit.INCH;

        System.out.println(LengthUnit.INCH.toString());

        LengthUnit meterUnit = LengthUnit.INCH;

        switch (meterUnit) {
            case FOOT:
                System.out.println("foot unit selected");
                break;
            case METER:
                System.out.println("meter unit selected");
                break;
            case INCH:
                System.out.println("inch unit selected");
                break;
            case CENTIMETER:
                System.out.println("centimeter unit selected");
                break;
            default:
                System.out.println("Please select a unit of measure");

        }
    }
}
