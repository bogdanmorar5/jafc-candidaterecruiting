package sda.example;

public enum LengthUnit {
    METER(1),
    CENTIMETER(0.01),
    FOOT(0.3),
    INCH(0.025),
    ;

    LengthUnit(double value) {  //am definit un constructor
    }
    double value;

    public void setValue(double value) {    //setter
        this.value = value;
    }


    double convertToMeters() {      //getter
        return value;
    }

    @Override
    public String toString() {
        return "LengthUnit{" +
                "value=" + value +
                '}';
    }
}
