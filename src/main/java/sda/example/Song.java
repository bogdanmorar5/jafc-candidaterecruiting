package sda.example;

public interface Song {

    public String getName();
    public int getLenght();

    public void play(Instrument instrument);

    default public int getNameLenght() {
        return getName().length();
    }

}
