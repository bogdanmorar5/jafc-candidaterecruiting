package sda.example.cuncurrency;

import java.util.Queue;

public class Manager extends Thread {

    private final ActiveEmployees atWork;

    public Manager() {
        atWork = new ActiveEmployees();
        atWork.employeeArrives("Bob");
        atWork.employeeArrives("Ana");
        atWork.employeeArrives("Dorel");
        atWork.employeeArrives("Pali");
        atWork.employeeArrives("Vali");
    }


    @Override
    public void run() {
        while(atWork.areEmployeesAtWork() == true) {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            atWork.employeeLeaves();
        }
    }
}
