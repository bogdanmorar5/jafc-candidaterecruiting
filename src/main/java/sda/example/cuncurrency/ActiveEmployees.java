package sda.example.cuncurrency;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.Queue;

public class ActiveEmployees {

    Queue<Employee> activeEmployees = new LinkedList<Employee>();   // le ia din "employee"

    public synchronized void employeeArrives(String name) {
        Employee newEmployee = new Employee(name);
        activeEmployees.add(newEmployee);
        newEmployee.start();
    }

    public synchronized void employeeLeaves() {     //daca vreau sa respect regula FIFO ramane gol, daca vrem unul specific lasam gol
       Employee leavingEmployee = activeEmployees.poll();   //pentru cand pleaca unul
        System.out.println(leavingEmployee.getIdname() + " its time to go home" + " " + Employee.time.format(LocalTime.now()));

        for(Employee currentEmployee : activeEmployees) {   //cand unul pleaca, restul isi dau speed up
            currentEmployee.speedUp();
        }
        leavingEmployee.interrupt();
    }

    public synchronized boolean areEmployeesAtWork() {
        return !activeEmployees.isEmpty();
    }

}
