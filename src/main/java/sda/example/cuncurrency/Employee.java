package sda.example.cuncurrency;

import lombok.AllArgsConstructor;
import lombok.Data;     // lombok ul ne permite sa folosim @Data samd

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Data   // gettere,settere fara a fi nevoie sa le facem noi
@AllArgsConstructor     //aduce constructori pentru fiecare si nu e nevoie noi sa facem construcotri


public class Employee extends Thread {

    final private String idname;
    private int workingSpeed = 10; // in sec
    public static final DateTimeFormatter time = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);

    public Employee(String idname) {
        this.idname = idname;
    }

    @Override
    public void run() {
        LocalTime timeToWork = LocalTime.now();     //DATA & ORA

        //       super.run();    // asta folosim sa executam ce e in clasa parinte dar noi NU vrem asta
        System.out.println(idname + " : I came to work at " + time.format(timeToWork));

        boolean working = true;

        while(working == true) {
            try {
                Thread.sleep(workingSpeed * 1000);
            } catch (InterruptedException e) {
        //        e.printStackTrace();    //daca se scoate asta nu mai apare in consola
                working = false;
            }
            System.out.println(idname + " : I am still working!");
        }
    }

    public synchronized void speedUp() {
        workingSpeed = workingSpeed - 2;    // decrementam cu doi ca trebuie sa acceleram procesul (cerinta problema)
    }
}
