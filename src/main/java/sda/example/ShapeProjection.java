package sda.example;

public class ShapeProjection {

    private int width;
    private int length;

    public ShapeProjection() {
    }

    public ShapeProjection(int width, int length) {
        this.width = width;
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "ShapeProjection{" +
                "width=" + width +
                ", length=" + length +
                '}';
    }

}
