package sda.example.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class MathOperations {

    public static void main(String[] args) {
        BiFunction<Integer, Integer, Integer> addition = (a,b) -> a + b;
        System.out.println(addition.apply(2,3));

        BinaryOperator<Integer> sub =(a, b) -> a - b;
        System.out.println(sub.apply(2,3));

        List<Integer> listOfInts = Collections.nCopies(4,10);   //de 5 ori valoarea 10
        int sum = 0;
        for(Integer intValue : listOfInts) {       // parcurgem colectia
            sum = addition.apply(sum, intValue);

        }
        System.out.println("Sum: " + sum);
    }

}
