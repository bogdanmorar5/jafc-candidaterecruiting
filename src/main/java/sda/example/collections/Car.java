package sda.example.collections;

import java.util.HashMap;
import java.util.Map;

public class Car {

    private String model;
    private int year;
    private Map<String, String> properties;

    public Car() {
        properties = new HashMap<>();
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void addProperties(String propertyName, String propertyValue) {
        this.properties.put(propertyName, propertyValue);
    }

}
