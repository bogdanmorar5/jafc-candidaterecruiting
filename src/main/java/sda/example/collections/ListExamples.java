package sda.example.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class ListExamples {
    public static void main(String[] args) {
        // citesc de la tastatura actiune executam: add = add element; delete = delete element; print = print list
        Scanner scanner = new Scanner(System.in);

        List<String> shoppingList = new ArrayList<>();

        Action action = null;
        do {
            action = getAction(scanner);
            switch (action) {
                case ADD:
                    // citim elementul
                    System.out.print("Shopping item: ");
                    String newItemToAdd = scanner.nextLine();
                    if (!shoppingList.contains(newItemToAdd)) {
                        shoppingList.add(newItemToAdd);
                    } else {
                        System.out.println("Item already exists!");
                    }
                    break;
                case DELETE:
                    // citim elementul
                    System.out.print("Shopping item: ");
                    String newItemToDelete = scanner.nextLine();
                    shoppingList.remove(newItemToDelete);
                    break;
                case PRINT:
                    System.out.print("Shopping list: ");
//                    for (String item : shoppingList) {
//                        System.out.print(item + ", ");
//                    }
                    Iterator<String> it = shoppingList.iterator();
                    while(it.hasNext()) {
                        System.out.print(it.next() + ", ");
                    }
                    System.out.println();
                    break;
            }
        } while (action != Action.EXIT);

        List<List<String>> itemsWithGroups = new ArrayList<>();
        // Integer[][] matrix = new Integer[10][10];

        itemsWithGroups.add(new ArrayList<String>());
        itemsWithGroups.get(0).add("mere");
        itemsWithGroups.get(0).add("pere");

        List<String> produseCosmetice = new ArrayList<String>();
        produseCosmetice.add("rimel");
        produseCosmetice.add("ruj");
        itemsWithGroups.add(produseCosmetice);
        itemsWithGroups.get(1).add("crema X");

    }

    private static Action getAction(Scanner scanner) {
        Action action = null;
        do {
            System.out.print("command: ");
            String command = scanner.nextLine();
            try {
                action = Action.valueOf(command.toUpperCase());
            } catch (IllegalArgumentException ex) {
                System.out.println("Please set a valid command!");
            }
        } while (action == null);

        return action;
    }

}
