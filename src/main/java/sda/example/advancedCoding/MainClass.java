package sda.example.advancedCoding;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import sda.example.advancedCoding.CandidateRecruiting.candidate.Candidate;
import sda.example.advancedCoding.CandidateRecruiting.recruitment.Company;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class MainClass {

    public static void main(String[] args) throws IOException {

//        Candidate candidate1 = new Candidate();
//        Candidate candidate2 = new Candidate("IOn popescu", 30, "Adress", "@yahoo.com", 3, 5, DepartmentName.PRODUCTION);
//        Candidate candidate3 = new Candidate("Ioana Mitre", 45, "Adress", "@yahoo.com", 8, 6, DepartmentName.MARKETING);
//        Candidate candidate4 = new Candidate("Dorin Tir", 23, "Adress", "@yahoo.com", 2, 4, DepartmentName.MARKETING);
//        Candidate candidate5 = new Candidate("Iasmina Grigore", 50, "Adress", "@yahoo.com", 8, 2, DepartmentName.PRODUCTION);

        List<Candidate> candidates = readCandidatesToJasonFile();       //citim din fisier si punem in fisier
//        candidates.add(candidate2);
//        candidates.add(candidate3);
//        candidates.add(candidate4);
//        candidates.add(candidate5);

        Company company = new Company("SDA Recruiting", candidates);
        company.recruiting();

        writeCandidatesToTxtFile(candidates);
        writeCandidatesToJasonFile(candidates);

    }

    public static void writeCandidatesToTxtFile(List<Candidate> candidates) {
        try (
                BufferedWriter writer = new BufferedWriter(new FileWriter("candidati - acceptati.txt"))
        ) {
            writer.write(candidates.toString());
        } catch (IOException e) {
            System.out.println("Could not write to file.");
        }
    }

    public static void writeCandidatesToJasonFile(List<Candidate> candidates) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File("candidati-acceptati.json"), candidates);

    }

    public static List<Candidate> readCandidatesToJasonFile() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Candidate> candidates =
                objectMapper.readValue(new File("candidati-initiali.json"), new TypeReference<List<Candidate>>() {
                });
        return candidates;

    }
}
