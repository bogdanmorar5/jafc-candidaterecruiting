package sda.example.advancedCoding.CandidateRecruiting.exceptions;

public class EvaluationIncapacityException extends RuntimeException {

    /**
     * checked exceptions must be declared (using throws) or treat using try-catch
     * unchecked exception = RuntimeExceptions
    * */


    public EvaluationIncapacityException() {
        super("This candidate didn't applied for for any valied position!");
    }
}
