package sda.example.advancedCoding.CandidateRecruiting.department;

import sda.example.advancedCoding.CandidateRecruiting.recruitment.Evaluator;

public abstract class Department implements Evaluator {

    protected DepartmentName name;
    protected int minLevelOfCompetence;

}
