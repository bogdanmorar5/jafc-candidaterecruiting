package sda.example.advancedCoding.CandidateRecruiting.department;

public enum DepartmentName {
    MARKETING,
    PRODUCTION,
    ACCOUNTANT,
    IT,
    HR
}
