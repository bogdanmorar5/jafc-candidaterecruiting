package sda.example.advancedCoding.CandidateRecruiting.department;


import sda.example.advancedCoding.CandidateRecruiting.candidate.Candidate;
import sda.example.advancedCoding.CandidateRecruiting.candidate.CandidateStatus;

public class Marketing extends Department {
    private static final int MIN_YEARS_OF_EXPERIENCE = 5;  // STATIC FINAL - nu se va mai putea schibma valoarea din ext

    public Marketing() {
        this.name = DepartmentName.MARKETING;
        this.minLevelOfCompetence = 7;

    }

    @Override
    public void evaluate(Candidate candidate) {
        if (candidate.getLevelofCompetence() > this.minLevelOfCompetence
        && candidate.getYearsOfExperience() > MIN_YEARS_OF_EXPERIENCE) {
            candidate.setCandidateStatus(CandidateStatus.ACCEPTED);
        } else {
            candidate.setCandidateStatus(CandidateStatus.REJECTED);
        }

    }

}



