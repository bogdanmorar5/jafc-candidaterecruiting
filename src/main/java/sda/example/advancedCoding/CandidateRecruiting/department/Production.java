package sda.example.advancedCoding.CandidateRecruiting.department;

import sda.example.advancedCoding.CandidateRecruiting.candidate.Candidate;
import sda.example.advancedCoding.CandidateRecruiting.candidate.CandidateStatus;

public class Production extends Department {

    public Production() {
        this.name = DepartmentName.PRODUCTION;
        this.minLevelOfCompetence = 5;
    }

    @Override
    public void evaluate(Candidate candidate) {
        if (candidate.getLevelofCompetence() > this.minLevelOfCompetence) {
            candidate.setCandidateStatus(CandidateStatus.ACCEPTED);
        } else {
            candidate.setCandidateStatus(CandidateStatus.REJECTED);
        }
    }
}
