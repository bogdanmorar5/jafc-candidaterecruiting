package sda.example.advancedCoding.CandidateRecruiting.candidate;

public enum CandidateStatus {

    ACCEPTED,
    REJECTED,
    PENDING
}
