package sda.example.advancedCoding.CandidateRecruiting.candidate;


import sda.example.advancedCoding.CandidateRecruiting.department.DepartmentName;

public class Candidate {

    private String name;
    private Integer age;
    private String address;
    private String emailAddress;
    private int levelofCompetence;
    private int yearsOfExperience;
    private DepartmentName departmentName;
    private CandidateStatus candidateStatus;


    public Candidate(String name,
                     Integer age,
                     String address,
                     String emailAddress,
                     int levelofCompetence,
                     int yearsOfExperience,
                     DepartmentName departmentName) {

        this.candidateStatus = CandidateStatus.PENDING;
        this.name = name;
        this.address = address;
        this.age = age;
        this.emailAddress = emailAddress;
        this.levelofCompetence = levelofCompetence;
        this.yearsOfExperience = yearsOfExperience;
        this.departmentName = departmentName;
    }

    public Candidate() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getLevelofCompetence() {
        return levelofCompetence;
    }

    public void setLevelofCompetence(int levelofCompetence) {
        this.levelofCompetence = levelofCompetence;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public DepartmentName getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(DepartmentName departmentName) {
        this.departmentName = departmentName;
    }

    public CandidateStatus getCandidateStatus() {
        return candidateStatus;
    }

    public void setCandidateStatus(CandidateStatus candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public String toString() {
        return "Candidate: " + name + " has " + yearsOfExperience +
                " years of experience, the level of competence " + levelofCompetence +
                " applied for " + departmentName +
                " and has the status " + candidateStatus
                + "\n";     //line seperator, pune urm. candidat pe o linie noua
    }
}
