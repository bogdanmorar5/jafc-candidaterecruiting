package sda.example.advancedCoding.CandidateRecruiting.recruitment;

import sda.example.advancedCoding.CandidateRecruiting.candidate.Candidate;
import sda.example.advancedCoding.CandidateRecruiting.department.DepartmentName;
import sda.example.advancedCoding.CandidateRecruiting.department.Marketing;
import sda.example.advancedCoding.CandidateRecruiting.department.Production;
import sda.example.advancedCoding.CandidateRecruiting.exceptions.EvaluationIncapacityException;

import java.util.List;

public class Company {

    private String name;
    private List<Candidate> candidates;

    public Company(String name, List<Candidate> candidates) {   //constrctorul avand numele si lista
        this.name = name;
        this.candidates = candidates;

    }

    public void recruiting() {
        // iteram peste lista (for each)
        Marketing marketingDep = new Marketing();
        Production productionDep = new Production();
        for(Candidate candidateList : candidates) {
            if (candidateList.getDepartmentName().equals(DepartmentName.PRODUCTION)) {
                productionDep.evaluate(candidateList);
            } else if(candidateList.getDepartmentName().equals(DepartmentName.MARKETING)) {
                marketingDep.evaluate(candidateList);
            } else {
            throw new EvaluationIncapacityException();      // works cause extends RUNTIMEEXCEPTION
            }
        }

    }


}
