package sda.example;

public class Piano implements Instrument {

    private Gama gama;

    @Override
    public void setGama(Gama gama) {
        this.gama = gama;
    }

    @Override
    public void playNote(Note note) {
        System.out.println("Piano-" + gama + "-" + note);
    }

}
